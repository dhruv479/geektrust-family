const fs = require('fs');

module.exports = {}.fileHandler = class {
  constructor(filePath) {
    this.filePath = filePath;
  }

  getFileContents() {
    return fs.readFileSync(this.filePath, 'utf8');
  }

  getFileContentLines() {
    const fileContent = fs.readFileSync(this.filePath, 'utf8');
    let fileData = fileContent.split('\n');
    if (fileData[fileData.length - 1] === '') {
      fileData.pop();
    }
    return fileData;
  }
};
