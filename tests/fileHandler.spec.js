const fileHandler = require('../fileHandler');

describe('"fileHandler Class Tests"', () => {
  const file = new fileHandler('./input1.txt');

  it('returns the content of file', () => {
    const fileData = file.getFileContents();
    expect(fileData).toEqual(
      'ADD_CHILD Satya Ketu Male\nGET_RELATIONSHIP Kriya Paternal-Uncle\nGET_RELATIONSHIP Satvy Brother-In-Law'
    );
  });

  it('returns the content of file line by line', () => {
    const fileContentLine = file.getFileContentLines();
    expect(fileContentLine[0]).toEqual('ADD_CHILD Satya Ketu Male');
  });
});
