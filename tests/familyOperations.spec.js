const familyOperations = require('../familyOperations');
const members = require('../family.json');

describe('"familyOperations Class Test"', () => {
  const family = new familyOperations(members);

  it('returns family members with filters', () => {
    const filterResponse = family.filterMembers([['id', 'Arit']]);
    expect(filterResponse).toEqual([
      {
        id: 'Arit',
        gender: 'Male',
        partnerId: 'Jnki'
      }
    ]);
  });

  it('returns Brothers of the member', () => {
    let member = { id: 'Vyas', motherId: 'Satya', gender: 'Male' };
    const brothers = family.getBrothers(member);
    expect(brothers).toEqual([
      {
        id: 'Asva',
        partnerId: 'Satvy',
        gender: 'Male',
        motherId: 'Satya',
        fatherId: 'Vyan'
      }
    ]);
  });

  it('returns Sisters of the member', () => {
    let member = {
      id: 'Vila',
      gender: 'Female',
      motherId: 'Lika',
      fatherId: 'Vich'
    };
    const sisters = family.getSisters(member);
    expect(sisters).toEqual([
      { id: 'Chika', gender: 'Female', motherId: 'Lika', fatherId: 'Vich' }
    ]);
  });

  it('adds a new child to the member', () => {
    expect(family.addMember('Lika', 'Sika', 'Male')).toBe(
      'CHILD_ADDITION_SUCCEEDED'
    );
  });

  it('returns Daughters of the member', () => {
    let member = {
      id: 'Aras',
      partnerId: 'Chitra',
      gender: 'Male',
      motherId: 'Anga',
      fatherId: 'Shan'
    };
    const daughters = family.getDaughter(member);
    expect(daughters).toEqual([
      {
        id: 'Jnki',
        gender: 'Female',
        partnerId: 'Arit',
        motherId: 'Chitra',
        fatherId: 'Aras'
      }
    ]);
  });

  it('returns Sons of the member', () => {
    let member = {
      id: 'Aras',
      partnerId: 'Chitra',
      gender: 'Male',
      motherId: 'Anga',
      fatherId: 'Shan'
    };
    const sons = family.getSon(member);
    expect(sons).toEqual([
      { id: 'Ahit', gender: 'Male', motherId: 'Chitra', fatherId: 'Aras' }
    ]);
  });

  it('returns Siblings of the member', () => {
    let member = {
      id: 'Vila',
      gender: 'Female',
      motherId: 'Lika',
      fatherId: 'Vich'
    };
    const siblings = family.getSiblings(member);
    expect(siblings).toEqual([
      { id: 'Chika', gender: 'Female', motherId: 'Lika', fatherId: 'Vich' },
      { id: 'Sika', motherId: 'Lika', fatherId: 'Vich', gender: 'Male' }
    ]);
  });

  it('returns Paternal-Uncle of the member', () => {
    let member = {
      id: 'Vasa',
      gender: 'Male',
      motherId: 'Satvy',
      fatherId: 'Asva'
    };
    const paternalUncle = family.getUncle(member, 'fatherId');
    expect(paternalUncle).toEqual([
      {
        id: 'Vyas',
        partnerId: 'Krpi',
        gender: 'Male',
        motherId: 'Satya',
        fatherId: 'Vyan'
      }
    ]);
  });

  it('returns Paternal-Aunt of the member', () => {
    let member = {
      id: 'Vasa',
      gender: 'Male',
      motherId: 'Satvy',
      fatherId: 'Asva'
    };
    const paternalAunt = family.getAunt(member, 'fatherId');
    expect(paternalAunt).toEqual([
      { id: 'Atya', gender: 'Female', motherId: 'Satya', fatherId: 'Vyan' }
    ]);
  });

  it('returns Maternal-Uncle of the member', () => {
    let member = {
      id: 'Yodhan',
      gender: 'Male',
      motherId: 'Dritha',
      fatherId: 'Jaya'
    };
    const maternalUncle = family.getUncle(member, 'motherId');
    expect(maternalUncle).toEqual([
      { id: 'Vritha', gender: 'Male', motherId: 'Amba', fatherId: 'Chit' }
    ]);
  });

  it('returns Maternal-Aunt of the member', () => {
    let member = {
      id: 'Yodhan',
      gender: 'Male',
      motherId: 'Dritha',
      fatherId: 'Jaya'
    };
    const maternalAunt = family.getAunt(member, 'motherId');
    expect(maternalAunt).toEqual([
      {
        id: 'Tritha',
        gender: 'Female',
        motherId: 'Amba',
        fatherId: 'Chit'
      }
    ]);
  });

  it('returns Bother-In-Law of the member', () => {
    let member = { id: 'Jaya', partnerId: 'Dritha', gender: 'Male' };
    const brotherInLaw = family.getInLaws(member, 'brother');
    expect(brotherInLaw).toEqual([
      { id: 'Vritha', gender: 'Male', motherId: 'Amba', fatherId: 'Chit' }
    ]);
  });

  it('returns Sister-In-Law of the member', () => {
    let member = { id: 'Jaya', partnerId: 'Dritha', gender: 'Male' };
    const sisterInLaw = family.getInLaws(member, 'sister');
    expect(sisterInLaw).toEqual([
      {
        id: 'Tritha',
        gender: 'Female',
        motherId: 'Amba',
        fatherId: 'Chit'
      }
    ]);
  });

  it('returns Relationship of the member', () => {
    let member = {
      id: 'Ish',
      gender: 'Male',
      motherId: 'Anga',
      fatherId: 'Shan'
    };
    const sister = family.getSisters(member);
    expect(sister).toEqual([
      {
        id: 'Satya',
        partnerId: 'Vyan',
        gender: 'Female',
        motherId: 'Anga',
        fatherId: 'Shan'
      }
    ]);
  });
});
