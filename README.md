# family-tree

> GeekTrust Backend Family Problem Solution

## Setup

```bash
# install dependencies
npm install

# run application
npm start input1.txt

# run test
npm run test
```

## files

# geektrust.js

- this file interfaces with the Class Objects, provide them input and console the output

# familyOperations.js

- this is a Class of all the operations/functions used to carry out a certain operation in the family tree. For adding more operations inherit the Class, override the getRelationship method and add other operations accordingly.

# fileHandler.js

- this is a Class to collect data from file and return the data.

# family.json

- this file contains all the relations stored in a Array of objects.
