const familyOperations = require('./familyOperations');
const fileHandler = require('./fileHandler');
const members = require('./family.json');

const file = new fileHandler(process.argv[2]);
const fileInput = file.getFileContentLines();

const family = new familyOperations(members);

fileInput.map(fileLine => {
  let lineSplit = fileLine.split(' ');
  const operation = lineSplit.shift();
  if (operation === 'ADD_CHILD') {
    console.log(family.addMember(...lineSplit));
  } else {
    [memberId, relation] = lineSplit;
    const member = family.filterMembers([['id', memberId]])[0];
    if (member !== undefined) {
      let relatives = family.getRelationship(member, relation);
      console.log(handleResponse(relatives));
    } else {
      console.log('PERSON_NOT_FOUND');
    }
  }
});

function handleResponse(memberArray) {
  if (memberArray.length === 0) {
    return 'NONE';
  }
  return memberArray
    .map(member => {
      return member.id;
    })
    .join(' ');
}
