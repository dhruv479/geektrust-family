const parentIdMap = {
  mother: 'motherId',
  father: 'fatherId'
};

const gender = {
  male: 'Male',
  female: 'Female'
};

const inLaw = {
  sister: 'sister',
  brother: 'brother'
};
module.exports = {}.familyOperations = class {
  constructor(members) {
    this.familyMembers = members;
  }

  filterMembers(filters) {
    return this.familyMembers.filter(function(val) {
      for (var i = 0; i < filters.length; i++)
        if (val[filters[i][0]] != filters[i][1]) return false;
      return true;
    });
  }

  getBrothers(member) {
    let memberBrothers = this.filterMembers([
      ['motherId', member.motherId],
      ['gender', gender.male]
    ]);
    if (member.gender === gender.male) {
      memberBrothers = memberBrothers.filter(brother => {
        return member.id === brother.id ? false : true;
      });
    }
    return memberBrothers;
  }

  getSisters(member) {
    let memberSisters = this.filterMembers([
      ['motherId', member.motherId],
      ['gender', gender.female]
    ]);
    if (member.gender === gender.female) {
      memberSisters = memberSisters.filter(sister => {
        return member.id === sister.id ? false : true;
      });
    }
    return memberSisters;
  }

  addMember(motherId, childId, childGender) {
    let mother = this.filterMembers([['id', motherId]])[0];
    if (mother === undefined) {
      return 'PERSON_NOT_FOUND';
    } else if (
      mother.gender === gender.male ||
      mother.partnerId === undefined
    ) {
      return 'CHILD_ADDITION_FAILED';
    } else {
      const newMember = {
        id: childId,
        motherId: motherId,
        fatherId: mother.partnerId,
        gender: childGender
      };
      this.familyMembers.push(newMember);
      return 'CHILD_ADDITION_SUCCEEDED';
    }
  }

  getSon(member) {
    return member.gender === gender.male
      ? this.filterMembers([
          ['fatherId', member.id],
          ['gender', gender.male]
        ])
      : this.filterMembers([
          ['motherId', member.id],
          ['gender', gender.male]
        ]);
  }

  getDaughter(member) {
    return member.gender === gender.male
      ? this.filterMembers([
          ['fatherId', member.id],
          ['gender', gender.female]
        ])
      : this.filterMembers([
          ['motherId', member.id],
          ['gender', gender.female]
        ]);
  }

  getSiblings(member) {
    if (member.motherId) {
      return [...this.getSisters(member), ...this.getBrothers(member)];
    } else {
      return [];
    }
  }

  getUncle(member, parentId) {
    let parent = this.filterMembers([['id', member[parentId]]])[0];
    if (parent.motherId) {
      return this.getBrothers(parent);
    } else {
      return [];
    }
  }

  getAunt(member, parentId) {
    let parent = this.filterMembers([['id', member[parentId]]])[0];
    if (parent.motherId) {
      return this.getSisters(parent);
    } else {
      return [];
    }
  }

  getInLaws(member, type) {
    let partnerSiblings = [],
      memberSiblingPartners = [];
    if (member.partnerId) {
      let partner = this.filterMembers([['id', member.partnerId]])[0];
      if (partner.motherId) {
        partnerSiblings =
          type === 'sister'
            ? this.getSisters(partner)
            : this.getBrothers(partner);
      }
    }
    if (member.motherId) {
      let memberSiblings =
        type === 'sister' ? this.getBrothers(member) : this.getSisters(member);
      memberSiblingPartners = memberSiblings.map(brother => {
        const brotherPartner = this.filterMembers([
          ['partnerId', brother.id]
        ])[0];
        if (brotherPartner !== null) {
          return brotherPartner;
        }
      });
    }
    return [...memberSiblingPartners, ...partnerSiblings];
  }

  getRelationship(member, type) {
    switch (type) {
      case 'Son':
        return this.getSon(member);

      case 'Daughter':
        return this.getDaughter(member);

      case 'Siblings':
        return this.getSiblings(member);

      case 'Paternal-Uncle':
        return this.getUncle(member, parentIdMap.father);

      case 'Paternal-Aunt':
        return this.getAunt(member, parentIdMap.father);

      case 'Maternal-Uncle':
        return this.getUncle(member, parentIdMap.mother);

      case 'Maternal-Aunt':
        return this.getAunt(member, parentIdMap.mother);

      case 'Brother-In-Law':
        return this.getInLaws(member, inLaw.brother);

      case 'Sister-In-Law':
        return this.getInLaws(member, inLaw.sister);

      case 'Brother':
        return this.getBrothers(member);

      case 'Sister':
        return this.getSisters(member);

      default:
        return [];
    }
  }
};
